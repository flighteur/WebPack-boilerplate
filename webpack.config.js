/* eslint-disable */
const path = require('path')
const glob = require('glob')
const wpOptions = require('./options')
const UglifyJS = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin')



console.log(wpOptions.wpPaths.jsFilePath)

//Compilation Options
const autoprefixer = true
const BSync = true

//------------- URL File configurations -----------------
let IMGRegExp = /\.(png|jpe?g|gif|svg)$/
let GRegExp = /\.(woff2?|eot|ttf|otf)(\?.*)?$/

let cssLoaders = [
  { loader: 'css-loader', options: { importLoaders: 1, minimize: !wpOptions.dev } }
]

if (autoprefixer) {
  cssLoaders.push({
    loader: 'postcss-loader',
    options: {
      plugins: (loader) => [
        require('autoprefixer')({
          browsers: ['last 2 versions']
        }),
      ]
    }
  })
}


let WebPackConfig = {
  entry: '.' + wpOptions.wpPaths.srcPath + 'js/app.js',
  watch: wpOptions.dev,
  output: {
    path: path.resolve('.' + wpOptions.wpPaths.distPath),
    publicPath: '/',
    filename: wpOptions.wpPaths.jsFilePath,
    chunkFilename: wpOptions.wpPaths.chunkFilePath
  },
  resolve: {
    alias: {
      '@assets': path.resolve('.' + wpOptions.wpPaths.assetsPath),
      '@css': path.resolve(wpOptions.wpPaths.cssPath),
      '@img': path.resolve(wpOptions.wpPaths.imgsPath),
      '@fonts': path.resolve(wpOptions.wpPaths.fontsPath),
      '@js': path.resolve(wpOptions.wpPaths.jsPath),
      '@jsc': path.resolve(wpOptions.wpPaths.jsPath + 'components/')
    }
  },
  devtool: wpOptions.dev ? "cheap-module-eval-source-map" : false,
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: ['eslint-loader']
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: cssLoaders
        })
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [...cssLoaders, 'sass-loader']
        })
      },
      {
        test: IMGRegExp,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: '[name].[hash:8].[ext]'
            }
          }
        ]
      },
      {
        test: GRegExp,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
      {
        test: /\.handlebars$/,
        use: ['handlebars-loader']
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: wpOptions.wpPaths.cssFilePath
    }),
    new ManifestPlugin(),
    new WriteFilePlugin(),
    new CleanWebpackPlugin(['.' + wpOptions.wpPaths.distPath], {
      root: path.resolve('./'),
      verbose: true,
    })
  ]
};

if (!wpOptions.dev) {
  WebPackConfig.plugins.push(new UglifyJS({
    sourceMap: false
  }))
  WebPackConfig.plugins.push(new CleanWebpackPlugin(['.' + wpOptions.wpPaths.distPath], {
    root: path.resolve('./'),
    verbose: true,
  }))
}

if (BSync) {
  WebPackConfig.plugins.push(new BrowserSyncPlugin(
    // BrowserSync options
    {
      // browse to http://localhost:3000/ during development
      host: 'localhost',
      port: 3000,
      // proxy the Webpack Dev Server endpoint
      // (which should be serving on http://localhost:3100/)
      // through BrowserSync
      proxy: 'http://localhost:8080/'
    },
    // plugin options
    {
      // prevent BrowserSync from reloading the page
      // and let Webpack Dev Server take care of this
      // or activate it to reload external browsers (such as mobile)
      reload: true
    }
  ))
}

const templatesFiles = glob.sync('.' + wpOptions.wpPaths.pagesPath + '*.hbs')
templatesFiles.forEach(file => {
  WebPackConfig.plugins.push(new HtmlWebpackPlugin({
    template: file,
    filename: './' + path.basename(file).replace('.hbs', '.html')
  }))
})
module.exports = WebPackConfig