let lib = {
  clone: (sel) => {
    import('./style.scss').then(() => {
      jq(sel).clone(false).appendTo($(sel).parent())
    })
  },
  switchbg: () => {
    switch (this.bgc) {
      case true:
        $('body').css('backgroundColor', '#def')
        this.bgc = false
        break
      default:
        $('body').css('backgroundColor', '#08F')
        this.bgc = true
        break
    }
  },
  bgc: false,
}

module.exports = lib
