let content = `<div>your content</div>
                    <div>your content</div>
                    <div>your content</div>`
let lib = {
  init: (sel) => {
    import('./lib/slick.js').then(() => {
      import('./lib/slick.scss').then(() => {
        import('./lib/slick-theme.scss').then(() => {
          $(sel).append(content)
          $(sel).slick()
        })
      })
    })
  },
}

module.exports = lib
