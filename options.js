/* eslint-disable */

const dev = process.env.NODE_ENV === "dev"

let wpPaths = new(function() {
  this.srcPath = '/src/' // Src path
  this.distPath = '/dist/' // Compiled path
  this.assetsPath = '/assets/'
  this.pagesPath = this.srcPath + 'pages/'

  //------------- JS Files configurations -----------------
  this.jsFilename = 'app' // Main app.js
  this.jsPath = '.' + this.assetsPath + 'js/'
  this.jsFilePath = dev ? this.jsPath + this.jsFilename + '.js' : this.jsPath + this.jsFilename + '[chunkhash:8].js' // Main app.js access path
  this.chunkFilePath = dev ? '.' + this.assetsPath + 'js/components/[id].[name].' + this.jsFilename + '.js' : '.' + this.assetsPath + 'js/components/[id].[name].[chunkhash:8].' + this.jsFilename + '.js' // Lazy Loading Modules access path

  //------------- CSS File configurations -----------------
  this.cssFilename = 'app',
  this.cssPath = '.' + this.assetsPath + 'css/'
  this.cssFilePath = dev ? this.cssPath + this.cssFilename + '.css' : this.cssPath + this.cssFilename + '.[contenthash:8].css' // Filepath to write the CSS File to the exact location needed

  //----------- Divers File configurations ------------------
  this.imgsPath = '.' + this.assetsPath + 'imgs/'
  this.fontsPath = '.' + this.assetsPath + 'fonts/'

  return this.srcPath,this.distPath,this.assetsPath,this.pagesPath,this.jsFilename,this.jsPath,this.jsFilePath,this.chunkFilePath,this.cssFilename,this.cssPath,this.cssFilePath,this.imgsPath,this.fontsPath
})()

module.exports = { wpPaths: wpPaths, dev: dev }