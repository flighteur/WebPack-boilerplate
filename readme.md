# WebPack Boilerplate

This boilerplate using WebPack allow you to use the latests standard in JScript (ECMA V6).
You can compile your sources using the module functionality included in JScript.

## Installation

If you want to install and use the stable version

```sh
git clone https://gitlab.com/flighteur/WebPack-boilerplate.git --single-branch
npm install
npm run dev
```

Or if you want the development version (May cause instability)

```sh
git clone -b develop https://gitlab.com/flighteur/WebPack-boilerplate.git --single-branch
npm install
npm run dev
```